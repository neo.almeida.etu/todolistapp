package com.example.maliste;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<TaskPojo> listOfTasks;
    private TaskAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.listOfTasks = new ArrayList<>();

        this.listView = findViewById(R.id.list_todo);
        adapter = new TaskAdapter(this, this.listOfTasks);
        this.listView.setAdapter(adapter);

    }

    public void deleteTask(View view)
    {
        View parent = (View) view.getParent();
        CheckBox text = (CheckBox) parent.findViewById(R.id.task_title);

        listOfTasks.removeIf(item -> item.getTaskDesc().equals(text.getText()));
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_add_task:
                EditText editText = new EditText(this);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Ajout d'une tache")
                        .setMessage("Message")
                        .setView(editText)
                        .setPositiveButton("Ajouter", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String task =   String.valueOf(editText.getText());
                                listOfTasks.add(new TaskPojo(task, false));
                            }
                        })
                        .setNegativeButton("Annuler", null)
                        .create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
        public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("taskList", this.listOfTasks);
        Log.d("SAVE", outState.getParcelableArrayList("taskList").toString());
    }

    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        Log.d("DELETE", "START METHOD");
        super.onRestoreInstanceState(savedInstanceState);
        assert savedInstanceState != null;
        this.listOfTasks = savedInstanceState.getParcelableArrayList("taskList");
        Log.d("DELETE", savedInstanceState.getParcelableArrayList("taskList").toString());
    }
}