package com.example.maliste;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class TaskPojo implements Parcelable {

    private Boolean finished;
    private String taskDesc;

    public TaskPojo(String taskDesc)
    {
        this.taskDesc = taskDesc;
        this.finished = false;
    }

    public TaskPojo(String taskDesc, boolean finished)
    {
        this.taskDesc = taskDesc;
        this.finished = finished;
    }

    protected TaskPojo(Parcel in) {
        byte tmpFinished = in.readByte();
        finished = tmpFinished == 0 ? null : tmpFinished == 1;
        taskDesc = in.readString();
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskPojo taskPojo = (TaskPojo) o;
        return Objects.equals(taskDesc, taskPojo.taskDesc);
    }



    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (finished == null ? 0 : finished ? 1 : 2));
        dest.writeString(taskDesc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaskPojo> CREATOR = new Creator<TaskPojo>() {

        @Override
        public TaskPojo createFromParcel(Parcel in) {
            return new TaskPojo(in);
        }

        @Override
        public TaskPojo[] newArray(int size) {
            return new TaskPojo[size];
        }

    };

}
