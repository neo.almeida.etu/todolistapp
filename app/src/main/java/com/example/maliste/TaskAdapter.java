package com.example.maliste;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class TaskAdapter extends ArrayAdapter<TaskPojo> {

    public TaskAdapter(Context context, ArrayList<TaskPojo> users)
    {
        super(context, 0, users);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        TaskPojo task = getItem(position);
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_todo, parent, false);
        }

        CheckBox textd = (CheckBox) convertView.findViewById(R.id.task_title);

        textd.setChecked(task.getFinished());
        textd.setText(task.getTaskDesc());

        return convertView;
    }
}
